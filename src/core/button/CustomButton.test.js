import React from "react";
import * as RN from "react-native";
import { create } from "react-test-renderer";
import { FAILURE, PRIMARY, SUCCESS } from "../../utils/constants";
import CustomButton from "./CustomButton";

describe("CustomButton", () => {
  it("should match snapshot", () => {
    const title = "Hello";
    let component = create(<CustomButton title={title} />);
    expect(component.root.findByType(RN.Text).props.children).toBe(title);

    expect(component).toMatchSnapshot(`for ${PRIMARY}`);

    component = create(<CustomButton title={title} type={SUCCESS} />);
    expect(component.root.findByType(RN.Text).props.children).toBe(title);

    expect(component).toMatchSnapshot(`for ${SUCCESS}`);

    component = create(<CustomButton title={title} type={FAILURE} />);
    expect(component.root.findByType(RN.Text).props.children).toBe(title);

    expect(component).toMatchSnapshot(`for ${FAILURE}`);
  });
});

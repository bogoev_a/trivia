import React from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  ViewPropTypes,
} from "react-native";
import theme from "../../theme/theme";
import { FAILURE, PRIMARY, SUCCESS } from "../../utils/constants";

const CustomButton = ({ title, type, style, ...props }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[styles.container(type), style]}
      {...props}
    >
      <Text style={styles.text}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: (type) => ({
    height: 48,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: theme.palette[type],
    borderRadius: 8,
  }),
  text: {
    ...theme.typography.largeText,
    textTransform: "uppercase",
    color: theme.palette.white,
  },
});

CustomButton.propTypes = {
  title: PropTypes.string.isRequired,
  type: PropTypes.oneOf([PRIMARY, SUCCESS, FAILURE]).isRequired,
  style: ViewPropTypes.style,
};

CustomButton.defaultProps = {
  type: PRIMARY,
};

export default CustomButton;

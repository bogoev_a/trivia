import React from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";
import theme from "../../theme/theme";

const CustomLoader = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator size="large" color={theme.palette.primary} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default CustomLoader;

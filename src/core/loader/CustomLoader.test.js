import React from "react";
import "react-native";
import { create } from "react-test-renderer";
import CustomLoader from "./CustomLoader";

describe("CustomLoader", () => {
  it("should match snapshot", () => {
    const component = create(<CustomLoader />);

    expect(component).toMatchSnapshot();
  });
});

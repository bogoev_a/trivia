import React from "react";
import { StyleSheet, ViewPropTypes } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

const CustomSafeAreaView = ({ style, ...props }) => {
  return <SafeAreaView style={[styles.container, style]} {...props} />;
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
  },
});

CustomSafeAreaView.propTypes = {
  style: ViewPropTypes.style,
};

export default CustomSafeAreaView;

import React from "react";
import * as RN from "react-native";
import { create } from "react-test-renderer";
import CustomSafeAreaView from "./CustomSafeAreaView";

describe("CustomSafeAreaView", () => {
  it("should match snapshot", () => {
    const component = create(
      <CustomSafeAreaView>
        <RN.Text>Hello</RN.Text>
      </CustomSafeAreaView>
    );

    expect(component).toMatchSnapshot();
  });
});

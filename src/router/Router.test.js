import React from "react";
import "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { act, create } from "react-test-renderer";
import { AppStack } from "./Router";
import Home from "../screens/home/Home";

describe("AppStack", () => {
  it("should match snapshot", () => {
    let component;
    act(() => {
      component = create(
        <NavigationContainer>
          <AppStack />
        </NavigationContainer>
      );
    });

    component.root.findByType(Home);

    expect(component).toMatchSnapshot();
  });
});

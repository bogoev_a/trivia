import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { SafeAreaProvider } from "react-native-safe-area-context";
import Home from "../screens/home/Home";
import { HOME_SCREEN, QUIZ_SCREEN, RESULTS_SCREEN } from "../utils/constants";
import Quiz from "../screens/quiz/Quiz";
import Results from "../screens/results/Results";

const Stack = createNativeStackNavigator();

export const AppStack = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen name={HOME_SCREEN} component={Home} />
    <Stack.Screen name={QUIZ_SCREEN} component={Quiz} />
    <Stack.Screen name={RESULTS_SCREEN} component={Results} />
  </Stack.Navigator>
);

const Router = () => {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <AppStack />
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default Router;

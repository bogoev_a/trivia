export const HELVETICA = "Helvetica";

export const PRIMARY = "primary";
export const SUCCESS = "success";
export const FAILURE = "failure";

export const HOME_SCREEN = "home";
export const QUIZ_SCREEN = "quiz";
export const RESULTS_SCREEN = "results";

export const TRUE = "True";
export const FALSE = "False";

import * as utils from "./utils";
import React from "react";
import checkPropTypes from "check-prop-types";

describe("stylePropType", () => {
  it("should be object, array of object or undefined", () => {
    const HelloComponent = () => <h1>Hi</h1>;

    HelloComponent.propTypes = {
      style: utils.stylePropType,
    };

    let result = checkPropTypes(
      HelloComponent.propTypes,
      { style: "Hello" },
      "prop",
      HelloComponent.name
    );

    expect(result).toBe(
      "Failed prop type: Invalid prop `style` supplied to `HelloComponent`, expected one of type [object, array]."
    );

    result = checkPropTypes(
      HelloComponent.propTypes,
      { style: undefined },
      "prop",
      HelloComponent.name
    );

    expect(result).toBeUndefined();
  });
});

describe("checkAnswer", () => {
  it("should return correct result", () => {
    const question = { answer: "tRue", correct_answer: "True" };

    expect(utils.checkAnswer(question)).toBe(true);

    question.answer = "False";

    expect(utils.checkAnswer(question)).toBe(false);

    question.correct_answer = "FaLse";

    expect(utils.checkAnswer(question)).toBe(true);
  });
});

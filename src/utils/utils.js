import PropTypes from "prop-types";

export const stylePropType = PropTypes.oneOfType([
  PropTypes.object,
  PropTypes.array,
]);

export const checkAnswer = (question) => {
  return (
    question.answer.toLowerCase() === question.correct_answer.toLowerCase()
  );
};

import React from "react";
import "react-native";
import { create } from "react-test-renderer";
import { useRoute, useNavigation } from "@react-navigation/core";
import Results from "./Results";
import ResultItem from "./resultitem/ResultItem";
import CustomButton from "../../core/button/CustomButton";
import { TRUE } from "../../utils/constants";

jest.mock("@react-navigation/core");

describe("Results", () => {
  it("should match snapshot and navigate when PLAY AGAIN is pressed", async () => {
    const firstQuestion = {
      question: "Question?",
      correct_answer: "True",
      answer: TRUE,
    };
    const secondQuestion = {
      question: "Question 2?",
      correct_answer: "False",
      answer: TRUE,
    };
    const questions = [firstQuestion, secondQuestion];

    useRoute.mockReturnValue({
      params: { questions },
    });

    const navigateMock = jest.fn();
    useNavigation.mockReturnValue({ navigate: navigateMock });

    const component = create(<Results />);

    expect(component.root.findAllByType(ResultItem).length).toBe(2);

    expect(component).toMatchSnapshot();

    expect(navigateMock).toBeCalledTimes(0);

    component.root.findByType(CustomButton).props.onPress();

    expect(navigateMock).toBeCalledTimes(1);
  });
});

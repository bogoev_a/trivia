import React from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import { useNavigation, useRoute } from "@react-navigation/native";
import CustomSafeAreaView from "../../core/safearea/CustomSafeAreaView";
import CustomButton from "../../core/button/CustomButton";
import { HOME_SCREEN } from "../../utils/constants";
import ResultItem from "./resultitem/ResultItem";
import theme from "../../theme/theme";
import { checkAnswer } from "../../utils/utils";

const renderItem = ({ item }) => <ResultItem question={item} />;

const extractKey = (item, index) => index.toString();

const renderSeparator = () => <View style={styles.separator} />;

const Results = () => {
  const questions = useRoute().params?.questions;
  const navigation = useNavigation();

  const correctAnswers = questions.filter((q) => checkAnswer(q)).length;

  return (
    <CustomSafeAreaView style={styles.container}>
      <Text style={styles.score}>You scored</Text>
      <Text
        style={styles.score}
      >{`${correctAnswers} / ${questions.length}`}</Text>
      <FlatList
        data={questions}
        renderItem={renderItem}
        keyExtractor={extractKey}
        ItemSeparatorComponent={renderSeparator}
        contentContainerStyle={styles.flatListContent}
        style={styles.scrollbarFix}
        ListFooterComponent={<View />}
        ListFooterComponentStyle={{
          marginBottom: theme.spacing.medium,
        }}
      />
      <CustomButton
        title="Play again?"
        onPress={() => navigation.navigate(HOME_SCREEN)}
        style={{ marginTop: theme.spacing.medium }}
      />
    </CustomSafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: theme.spacing.medium,
  },
  score: {
    ...theme.typography.verylargeText,
    color: theme.palette.veryDarkGrey,
    textAlign: "center",
  },
  separator: {
    height: theme.spacing.small,
  },
  scrollbarFix: {
    marginHorizontal: -theme.spacing.medium,
    paddingHorizontal: theme.spacing.medium,
  },
  flatListContent: {
    flexGrow: 1,
  },
});

export default Results;

import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, Text, View, ViewPropTypes } from "react-native";
import { checkAnswer, stylePropType } from "../../../utils/utils";
import theme from "../../../theme/theme";
import CheckIcon from "../../../../assets/svg/check.svg";
import XIcon from "../../../../assets/svg/x.svg";

const ResultItem = ({ question, style, ...props }) => {
  const isCorrect = checkAnswer(question);

  const iconProps = { width: 16, height: 16, style: styles.icon };

  return (
    <View style={[styles.container, style]} {...props}>
      {isCorrect ? <CheckIcon {...iconProps} /> : <XIcon {...iconProps} />}
      <Text style={styles.text(isCorrect)}>{question.question}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
  text: (isCorrect) => ({
    flex: 1,
    ...theme.typography.largeText,
    color: isCorrect ? theme.palette.success : theme.palette.failure,
  }),
  icon: {
    marginTop: 5,
    marginRight: theme.spacing.small,
  },
});

ResultItem.propTypes = {
  style: ViewPropTypes.style,
  question: PropTypes.shape({
    question: PropTypes.string.isRequired,
    answer: PropTypes.string.isRequired,
    correct_answer: PropTypes.string.isRequired,
  }).isRequired,
};

export default ResultItem;

import React from "react";
import "react-native";
import { create } from "react-test-renderer";
import ResultItem from "./ResultItem";
import CheckIcon from "../../../../assets/svg/check.svg";
import XIcon from "../../../../assets/svg/x.svg";

describe("CustomSafeAreaView", () => {
  it("should match snapshot", () => {
    const question = {
      question: "Question?",
      correct_answer: "False",
      answer: "FaLse",
    };
    let component = create(<ResultItem question={question} />);

    component.root.findByType(CheckIcon);
    expect(() => {
      component.root.findByType(XIcon);
    }).toThrow();

    expect(component).toMatchSnapshot("with correct answer");

    question.correct_answer = "True";
    component = create(<ResultItem question={question} />);

    component.root.findByType(XIcon);
    expect(() => {
      component.root.findByType(CheckIcon);
    }).toThrow();

    expect(component).toMatchSnapshot("with incorrect answer");
  });
});

import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import CustomSafeAreaView from "../../core/safearea/CustomSafeAreaView";
import { fetchQuestions } from "../../http/get";
import theme from "../../theme/theme";
import CustomButton from "../../core/button/CustomButton";
import {
  FAILURE,
  FALSE,
  RESULTS_SCREEN,
  SUCCESS,
  TRUE,
} from "../../utils/constants";
import { useNavigation } from "@react-navigation/native";
import CustomLoader from "../../core/loader/CustomLoader";

const Quiz = () => {
  const [questions, setQuestions] = useState([]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const navigation = useNavigation();
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {
    (async () => {
      try {
        setQuestions(await fetchQuestions());
      } catch (error) {
        console.log(error);
      } finally {
        setIsReady(true);
      }
    })();
  }, []);

  const answerQuestion = (answer) => {
    const copy = [...questions];
    copy[currentIndex].answer = answer;
    setQuestions(copy);
    if (currentIndex < questions.length - 1) {
      setCurrentIndex(currentIndex + 1);
    } else {
      navigation.navigate(RESULTS_SCREEN, { questions });
    }
  };

  const currentQuestion = questions[currentIndex];

  return (
    <CustomSafeAreaView style={styles.flex}>
      {isReady && currentQuestion ? (
        <ScrollView contentContainerStyle={styles.container}>
          <Text style={styles.category}>{currentQuestion.category}</Text>
          <View style={styles.midContainer}>
            <View style={styles.questionBox}>
              <Text style={styles.questionText}>
                {currentQuestion.question}
              </Text>
            </View>
            <Text style={styles.answeredQuestions}>{`${currentIndex + 1} of ${
              questions.length
            }`}</Text>
          </View>
          <View>
            <CustomButton
              style={styles.firstButton}
              title={TRUE}
              type={SUCCESS}
              onPress={() => answerQuestion(TRUE)}
            />
            <CustomButton
              title={FALSE}
              type={FAILURE}
              onPress={() => answerQuestion(FALSE)}
            />
          </View>
        </ScrollView>
      ) : (
        <CustomLoader />
      )}
    </CustomSafeAreaView>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    flexGrow: 1,
    paddingHorizontal: theme.spacing.medium,
  },
  category: {
    ...theme.typography.verylargeText,
    textAlign: "center",
  },
  midContainer: {
    flex: 1,
    justifyContent: "center",
  },
  questionBox: {
    padding: theme.spacing.small,
    borderWidth: 2,
    borderColor: theme.palette.veryDarkGrey,
    borderRadius: theme.borderRadius,
  },
  questionText: {
    ...theme.typography.largeText,
    color: theme.palette.veryDarkGrey,
    textAlign: "center",
  },
  firstButton: {
    marginBottom: theme.spacing.small,
  },
  answeredQuestions: {
    marginTop: theme.spacing.small,
    textAlign: "center",
  },
});

export default Quiz;

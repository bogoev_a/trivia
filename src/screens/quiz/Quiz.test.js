import React from "react";
import "react-native";
import { act, create } from "react-test-renderer";
import Quiz from "./Quiz";
import { fetchQuestions } from "../../http/get";
import { FALSE, RESULTS_SCREEN, TRUE } from "../../utils/constants";
import { useNavigation } from "@react-navigation/core";

jest.mock("@react-navigation/core");
jest.mock("../../http/get.js");

describe("Quiz", () => {
  const firstQuestion = { question: "Question?", correct_answer: "True" };
  const secondQuestion = { question: "Question 2?", correct_answer: "False" };
  const questions = [firstQuestion, secondQuestion];

  it("should match snapshot", async () => {
    fetchQuestions.mockResolvedValueOnce(questions);

    let component;
    await act(async () => {
      component = create(<Quiz />);
    });

    expect(component).toMatchSnapshot("with questions");

    fetchQuestions.mockRejectedValueOnce(new Error("Err"));

    await act(async () => {
      component = create(<Quiz />);
    });

    expect(component).toMatchSnapshot("without questions");
  });

  it("should navigate to Results if there are no more questions", async () => {
    fetchQuestions.mockResolvedValueOnce(questions);
    const navigateMock = jest.fn();
    useNavigation.mockReturnValue({ navigate: navigateMock });

    let component;
    await act(async () => {
      component = create(<Quiz />);
    });

    expect(navigateMock).not.toHaveBeenCalled();

    component.root.findByProps({ children: firstQuestion.question });

    await act(async () => {
      await component.root.findByProps({ title: TRUE }).props.onPress();
      await component.root.findByProps({ title: FALSE }).props.onPress();
    });

    expect(navigateMock).toHaveBeenCalledTimes(1);

    questions[0].answer = TRUE;
    questions[1].answer = FALSE;
    expect(navigateMock).toHaveBeenCalledWith(RESULTS_SCREEN, {
      questions: questions,
    });
  });

  it("should change questions if there are more left", async () => {
    fetchQuestions.mockResolvedValueOnce(questions);

    let component;
    await act(async () => {
      component = create(<Quiz />);
    });

    component.root.findByProps({ children: firstQuestion.question });

    act(() => {
      component.root.findByProps({ title: TRUE }).props.onPress();
    });

    component.root.findByProps({ children: secondQuestion.question });
  });
});

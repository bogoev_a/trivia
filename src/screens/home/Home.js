import { useNavigation } from "@react-navigation/native";
import React from "react";
import { StyleSheet, Text } from "react-native";
import CustomButton from "../../core/button/CustomButton";
import CustomSafeAreaView from "../../core/safearea/CustomSafeAreaView";
import theme from "../../theme/theme";
import { QUIZ_SCREEN } from "../../utils/constants";

const Home = () => {
  const navigation = useNavigation();

  return (
    <CustomSafeAreaView style={styles.container}>
      <Text style={styles.header}>Welcome to the Trivia Challenge</Text>
      <Text style={styles.description}>
        You will be presented with 10 True or False questions
      </Text>
      <Text style={styles.description}>Can you score 100%?</Text>
      <CustomButton
        title="Begin"
        onPress={() => navigation.navigate(QUIZ_SCREEN)}
      />
    </CustomSafeAreaView>
  );
};

const common = {
  color: theme.palette.veryDarkGrey,
  textAlign: "center",
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-between",
    flex: 1,
    paddingHorizontal: theme.spacing.medium,
  },
  header: {
    ...theme.typography.verylargeText,
    ...common,
  },
  description: {
    ...theme.typography.largeText,
    ...common,
  },
});

export default Home;

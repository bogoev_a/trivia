import React from "react";
import "react-native";
import { create } from "react-test-renderer";
import Home from "./Home";
import { useNavigation } from "@react-navigation/core";
import CustomButton from "../../core/button/CustomButton";
import { QUIZ_SCREEN } from "../../utils/constants";

jest.mock("@react-navigation/core");

describe("CustomSafeAreaView", () => {
  it("should match snapshot", () => {
    const navigateMock = jest.fn();
    useNavigation.mockReturnValue({ navigate: navigateMock });

    const component = create(<Home />);

    expect(navigateMock).not.toHaveBeenCalled();

    component.root.findByType(CustomButton).props.onPress();

    expect(navigateMock).toBeCalledWith(QUIZ_SCREEN);

    expect(component).toMatchSnapshot();
  });
});

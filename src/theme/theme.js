import { FAILURE, HELVETICA, PRIMARY, SUCCESS } from "../utils/constants";

const palette = {
  [PRIMARY]: "#155799",
  [SUCCESS]: "#159957",
  [FAILURE]: "#ea4f48",
  white: "#fff",
  transparent: "transparent",
  veryDarkGrey: "#24292f",
};

const theme = {
  palette,
  typography: {
    verylargeText: {
      fontFamily: HELVETICA,
      fontSize: 24,
      lineHeight: 36,
      fontWeight: "600",
    },
    largeText: {
      fontFamily: HELVETICA,
      fontSize: 16,
      lineHeight: 24,
      fontWeight: "400",
    },
    regularText: {
      fontFamily: HELVETICA,
      fontSize: 14,
      lineHeight: 21,
      fontWeight: "400",
    },
    regularTextBold: {
      fontFamily: HELVETICA,
      fontSize: 14,
      lineHeight: 20,
      fontWeight: "600",
    },
    smallText: {
      fontFamily: HELVETICA,
      fontSize: 12,
      lineHeight: 18,
    },
  },
  borderRadius: 8,
  spacing: {
    small: 10,
    medium: 20,
    large: 30,
  },
};

export default theme;

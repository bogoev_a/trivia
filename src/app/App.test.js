import React from "react";
import "react-native";
import { create } from "react-test-renderer";
import App from "./App";

import { useFonts } from "expo-font";
import Router from "../router/Router";
import AppLoading from "expo-app-loading";

jest.mock("expo-font");

describe("App", () => {
  it("should match snapshot", () => {
    useFonts.mockReturnValueOnce([false]);

    let component = create(<App />);

    component.root.findByType(AppLoading);

    useFonts.mockReturnValueOnce([true]);

    component = create(<App />);

    component.root.findByType(Router);
  });
});

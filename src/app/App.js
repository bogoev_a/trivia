import React from "react";
import AppLoading from "expo-app-loading";
import { useFonts } from "expo-font";
import Router from "../router/Router";

const App = () => {
  const [fontsLoaded] = useFonts({
    Helvetica: require("../../assets/fonts/Helvetica.ttf"),
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return <Router />;
};

export default App;

import axiosInstance from "./axiosInstance";
import { fetchQuestions } from "./get";

jest.mock("./axiosInstance");

describe("fetchQuestions", () => {
  it("should return correct decoded response", async () => {
    const questions = [
      { id: 1, question: "John&#39;s son", correct_answer: "True" },
    ];
    const res = {
      data: { results: questions },
    };
    axiosInstance.get.mockResolvedValueOnce(res);

    const result = await fetchQuestions();

    expect(axiosInstance.get).toBeCalledWith("/", {
      params: { amount: 10, difficulty: "hard", type: "boolean" },
    });

    questions[0].question = "John's son";
    expect(result).toEqual(questions);
  });

  it("should throw error on api call error", async () => {
    const err = new Error("Network Error");
    axiosInstance.get.mockRejectedValueOnce(err);

    try {
      await fetchQuestions();
    } catch (error) {
      expect(error).toEqual(err);
    }
  });
});

import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "https://opentdb.com/api.php",
  timeout: 3000,
});

export default axiosInstance;

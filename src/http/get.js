import axiosInstance from "./axiosInstance";
import he from "he";

export const fetchQuestions = async () => {
  const res = await axiosInstance.get("/", {
    params: { amount: 10, difficulty: "hard", type: "boolean" },
  });

  return res.data.results.map((q) => ({
    ...q,
    question: he.decode(q.question),
  }));
};
